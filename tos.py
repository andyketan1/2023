from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

# Konfigurasi WebDriver
driver = webdriver.Chrome()

# Akses Facebook
driver.get("https://www.facebook.com")

# Login
email = driver.find_element_by_name("email")
password = driver.find_element_by_name("pass")

email.send_keys("your-email@example.com")
password.send_keys("your-password")
password.send_keys(Keys.RETURN)

# Tunggu beberapa detik untuk login
time.sleep(10)

# Akses halaman yang ingin Anda interaksi
driver.get("https://www.facebook.com/your-page-or-post")

# Contoh: klik tombol like (jika ada tombol yang ingin diklik)
try:
    like_button = driver.find_element_by_xpath("//span[text()='Like']")
    like_button.click()
except Exception as e:
    print(f"Error: {e}")

# Tunggu dan tutup browser
time.sleep(5)
driver.quit()