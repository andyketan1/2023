from selenium import webdriver
import time

# Path to ChromeDriver (replace with the path on your system)
chrome_path = "/path/to/chromedriver"

# Number of iterations
num_iterations = 100

# Initialize the driver
driver = webdriver.Chrome
# Loop through the specified number of times
for _ in range(num_iterations):
    # Open the website
    driver.get("https://septyshop.com")

    # Wait for a few seconds (you can adjust the sleep duration if needed)
    time.sleep(5)

# Close the browser
driver.quit()
